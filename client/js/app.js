// App
const app = angular.module('app', []);

// App controller
app.controller('appController', ['$scope','ws_client','$timeout', ($scope,ws_client,$timeout,$anchorScroll,$location) => {
	$scope.state = {
			messages:[],
			post:{avatar:'man',username:'',question:'',canEdit:false}
	};
	$scope.onNewMessage = (payload) => {
		$timeout(() => {
				switch (payload.messageType) {
					case 'initDone':
						$scope.state.post.username = payload.username;
						$scope.state.messages.push(payload.message);
						break;
					default:
						$scope.state.messages.push(payload);
				}

		})
		$timeout(() => {
			var div = document.getElementsByClassName('scrollview')[0];
			div.scrollTop = div.scrollHeight - div.clientHeight;
		},500)
	}
	$scope.fixAnswer = (item) => {
		item.setNewAnswer = false;
		let newItem = angular.copy(item);
		newItem.username = $scope.state.post.username;
		newItem.avatar = 'man';
		$scope.publish(newItem);
	}
	$scope.publish = (post) => {
		if(post.question != ''){
			ws_client.publish(post);
			post.question = '';
		}
	}
	ws_client.connect($scope.onNewMessage);
}]);
