function WsClient(){
	const URL = "ws://localhost:3000";
	this.ws = {};
	this.onMessage = null;
	this.connect = () => {
		this.ws = new WebSocket(URL);
		this.ws.onopen = (() => {
			console.log('open');
		}).bind(this);
		this.ws.onmessage = ({data}) => {
			data = JSON.parse(data);
			this.onMessage(data);
		}
	}
	this.publish = (data) => {
		this.ws.send(JSON.stringify(data));
	}
}
