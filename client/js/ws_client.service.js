app.factory('ws_client', [() => {
	this.ws = {};
	return {
		connect : (callback) =>{
			this.ws = new WsClient();
			this.ws.onMessage = (message) => {
				callback(message);
			}
			ws.connect();
		},
		publish : (data) => {
			this.ws.publish(data);
		}
	}
}]);
