const express   = require('express');
const http = require('http');
const uuid = require('uuid/v1');
const app       = express();
const port      = 3000;
const WebSocket = require('ws');
const BigMind = require('./server/bigMind.js');

app.use(express.static(`${__dirname}/client`)); 		// statics
require(`./server/routes.js`)(app);						// routes

function initServer(){
  var bot = new BigMind();
  const server = http.createServer(app);
  const wss = new WebSocket.Server({ server });

  wss.on('connection', function connection(ws, req) {
    ws.isAlive = true;
    ws.on('pong', function heartbeat() {
      this.isAlive = true;
    });
    ws.send(JSON.stringify(bot.welcomeMessage()));
    ws.on('message', async function incoming(data) {
      try{
        var payload = JSON.parse(data);
        payload.id = uuid();
        //post the user's fixed answer and teach bigMind
        if(payload.newAnswer){
          payload.answer = payload.newAnswer;
          bot.teach(payload.question,payload.newAnswer);
          payload.question = payload.newAnswer;
          wss.broadcast(payload);
        }
        //bigMind will try to answer
        else{
          //broadcast user's question to everyone
          wss.broadcast(payload);
          let answer = await bot.ask(payload);
          //broadcast bot answare
          wss.broadcast(answer);
        }
      }
      catch(err){
        console.log(err);
      }
    });
  });
  wss.broadcast = function broadcast(data) {
    data =  JSON.stringify(data);
    for(client of wss.clients){
      if (client.readyState === WebSocket.OPEN) {
        client.send(data);
      }
    }
  };
  server.listen(port, function listening() {
    console.log('Listening on %d', server.address().port);
  });
  setInterval(() => {
    wss.clients.forEach(function each(ws) {
      if (ws.isAlive === false) return ws.terminate();

      ws.isAlive = false;
      ws.ping('', false, true);
    });
  }, 30000);
}

initServer();
