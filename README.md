![alt text](https://image.freepik.com/free-vector/brain-character_23-2147517600.jpg)
# BigMind

BigMind is a smart chat bot that realy wants to learn.
Trained with questions from the famous Jeopardy TV show, BigMind knows a thing or two :).

BigMind uses a BayesClassifier that enables him to learn and evolve.

```sh
download Jeopardy question pool: 
https://drive.google.com/file/d/0BwT5wj_P7BKXb2hfM3d2RHU1ckE/view
```
### Installation

```sh
$ npm i
$ npm start
```
### Development

```sh
$ npm run dev
```
```sh
client runs on http://localhost:3000
```

### Playing with BigMind
* Ask questions
* Tell BigMind when he is wrong and give him the right answer
* Ask for some "hacker" tips from BigMind