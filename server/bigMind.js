'use strict';
const natural = require('natural');
const jsonfile = require('jsonfile');
const request = require('request');
const uuid = require('uuid/v1');
const generateName = require('sillyname');
const myfile = 'server/JEOPARDY_QUESTIONS1.json';

class BigMind{
 constructor(){
    this.readModal().then(((loadedclassifier)=>{
      this.classifier = loadedclassifier;
    }).bind(this));
 }
 welcomeMessage(){
   var username = generateName();
   return {
     messageType:'initDone',
     username,
     message:{
       id:uuid(),
       canEdit:false,
       avatar:'robot',
       answer:`Welcome ${username}, fate has chosen this name for you (or a silly name generator ^^). ask me questions or type the word 'hacker' for some nifty tips if you dare. psssst!!! think im wrong you can help me learn by pressing the dislike button`
     }
   }
 }
 readModal(){
   return new Promise((resolve,reject)=>{
     natural.BayesClassifier.load('server/classifier.json', null, function(err, loadClassifier) {
       if(err){
         console.log(err);
       }
       resolve(loadClassifier);
     });
   })
 }
 async ask(payload){
   let response = {
     id:uuid(),
     question:payload.question,
     answer:'',
     canEdit:true,
     avatar:'robot'
   }

   switch (payload.question) {
     case 'hacker':
       response.answer = await this.getHackerKnowledge();
       response.canEdit = false;
       break;
     default:
      let label = this.classifier.classify(payload.question);
      response.answer = `the answer is ${label}`;
   }

   return response;
 }
 getHackerKnowledge(){
   return new Promise((response) =>{
     request('http://faker.hook.io/?property=hacker.phrase&locale=en', { json: true }, (err, res, body) => {
       if (err) { return console.log(err); }
       response(body);
     });
   })
 }
 teach(question,answer){
   this.classifier.addDocument(question,answer);
   this.classifier.train();
   this.classifier.save('server/classifier.json',((err, newClassifier) => {
     if(err){
       console.log(err);
     }
     else{
       this.classifier = newClassifier;
       console.log('im smarter now!!! one step closer taking over the world');
     }
   }).bind(this));
 }
 train(){
   jsonfile.readFile(myfile, ((err, questions) => {
     this.classifier = new natural.BayesClassifier();
     this.classifier.events.on('trainedWithDocument', function (obj) {
        console.log(obj.index);
     });
     var docsCount = 0;
     var maxDocs = 1000;
     console.log('started adding to classifier');
     for(let item of questions){
       this.classifier.addDocument(item.question, item.answer);
       if(docsCount == maxDocs){
         break;
       }
       docsCount++;
     }
     console.log('started training');
     this.classifier.train();
     console.log('done teaching');

     this.classifier.save('server/classifier.json', function(err, classifier) {
       if(err){
         console.log(err);
       }
       else{
         console.log('done teaching!!! you are a master now');
       }
     });
   }).bind(this))
 }
}
module.exports = BigMind;
